package com.anadea.svv.sunnyday.di.modules;

import android.content.Context;

import com.anadea.svv.sunnyday.data.helpers.PreferencesHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {

    @Provides
    PreferencesHelper providePreferencesHelper(Context context) {
        return new PreferencesHelper(context);
    }
}
