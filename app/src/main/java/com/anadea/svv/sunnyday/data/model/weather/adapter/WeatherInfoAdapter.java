package com.anadea.svv.sunnyday.data.model.weather.adapter;

import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WeatherInfoAdapter implements WeatherInfoModel {

    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm", Locale.US);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    private static final SimpleDateFormat DATE_EXT_FORMAT = new SimpleDateFormat("EEEE, d MMM yyyy", Locale.getDefault());

    private final String locationName;
    private final WeatherInfo weatherInfo;

    public WeatherInfoAdapter(String locationName, WeatherInfo weatherInfo) {
        this.locationName = locationName;
        this.weatherInfo = weatherInfo;
    }

    public boolean isEmpty() {
        return locationName == null && weatherInfo == null;
    }

    @Override
    public String getLocationName() {
        return locationName;
    }

    @Override
    public String getTemperature() {

        int temp = (int) weatherInfo.getCommon().getTemperature();
        String plusSign = temp > 0 ? "+" : "";
        return String.format(Locale.getDefault(), "%s%d", plusSign, temp);
    }

    @Override
    public String getWeatherDate() {
        return DATE_FORMAT.format(new Date(weatherInfo.getTimestamp() * 1000));
    }

    @Override
    public String getWeatherDateExt() {
        return DATE_EXT_FORMAT.format(new Date(weatherInfo.getTimestamp() * 1000));
    }

    @Override
    public String getWeatherTime() {
        return TIME_FORMAT.format(new Date(weatherInfo.getTimestamp() * 1000));
    }

    @Override
    public String getIconPath() {
        return (weatherInfo.getDescriptionList().isEmpty())
                ? ""
                : "http://openweathermap.org/img/w/" + weatherInfo.getDescriptionList().get(0).getIcon() + ".png";
    }

    @Override
    public String getMaxTemperature() {
        return String.valueOf((int) weatherInfo.getCommon().getTemperatureMax());
    }

    @Override
    public String getMinTemperature() {
        return String.valueOf((int) weatherInfo.getCommon().getTemperatureMin());
    }

    @Override
    public String getHumidity() {
        return String.format(Locale.US, "%d%%", (int) weatherInfo.getCommon().getHumidity());
    }

    @Override
    public String getWindSpeed() {
        return String.valueOf((int) weatherInfo.getWind().getSpeed());
    }

    @Override
    public String getPressure() {
        return String.valueOf((int) weatherInfo.getCommon().getPressure());
    }

    @Override
    public String getDescription() {

        String desc = (weatherInfo.getDescriptionList().isEmpty())
                ? null
                : weatherInfo.getDescriptionList().get(0).getDescription();

        if (desc != null && desc.length() > 0)
            desc = desc.substring(0, 1).toUpperCase() + desc.substring(1);

        return desc;
    }

    public static List<WeatherInfoAdapter> transformWeatherInfoList(String locationName,
                                                                    List<WeatherInfo> weatherInfoList) {

        List<WeatherInfoAdapter> result = new ArrayList<>();
        for (WeatherInfo weatherInfo : weatherInfoList)
            result.add(new WeatherInfoAdapter(locationName, weatherInfo));

        return result;
    }

}
