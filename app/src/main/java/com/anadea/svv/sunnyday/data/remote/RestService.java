package com.anadea.svv.sunnyday.data.remote;

import com.anadea.svv.sunnyday.data.model.forecast.ForecastInfo;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestService {

    private static final String TAG = "RestService";

    private final OpenWeatherApi openWeatherApi;

    public RestService() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new OpenWeatherInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        openWeatherApi = retrofit.create(OpenWeatherApi.class);
    }

    public Single<WeatherInfo> requestCurrentWeather(double latitude, double longitude) {
        return openWeatherApi.getCurrentWeather(latitude, longitude);
    }

    public Single<ForecastInfo> requestFiveDaysForecast(double latitude, double longitude) {
        return openWeatherApi.getFiveDaysForecast(latitude, longitude);
    }

    public Single<WeatherInfo> requestCurrentWeatherByName(String name) {
        return openWeatherApi.getCurrentWeatherByName(name);
    }

}
