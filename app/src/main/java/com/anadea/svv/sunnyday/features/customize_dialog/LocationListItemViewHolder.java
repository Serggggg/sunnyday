package com.anadea.svv.sunnyday.features.customize_dialog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.anadea.svv.sunnyday.R;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

class LocationListItemViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    interface ItemClickListener {
        void onListItemClick(View view, int position);
    }

    private final WeakReference<ItemClickListener> itemClickListenerReference;

    @BindView(R.id.location_list_item_title_textView)
    TextView titleTextView;
    @BindView(R.id.location_list_item_check_imageView)
    ImageView checkedImageView;

    LocationListItemViewHolder(View itemView,
                               WeakReference<ItemClickListener> itemClickListenerReference) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);

        this.itemClickListenerReference = itemClickListenerReference;
    }

    @Override
    public void onClick(View view) {
        if (itemClickListenerReference != null)
            itemClickListenerReference.get().onListItemClick(view, getAdapterPosition());
    }
}
