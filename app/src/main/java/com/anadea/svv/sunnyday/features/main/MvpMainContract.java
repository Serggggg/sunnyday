package com.anadea.svv.sunnyday.features.main;

import android.app.Activity;
import android.view.MenuItem;

import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoModel;
import com.anadea.svv.sunnyday.mvp.core.IPresenter;
import com.anadea.svv.sunnyday.mvp.core.IView;

import java.util.List;

public interface MvpMainContract {

    interface View extends IView{

        void onError(String message);

        void startProgress();

        void stopProgress();

        void checkLocationPermissions();

        void updateNavigationDrawerMenu(List<LocationInfo> userLocationsList);

        void updateNavHeaderLocationData(LocationInfo locationInfo);

        void updateCurrentWeatherData(WeatherInfoModel weatherInfoModel);

        void setAddToFavButtonVisible(boolean enableAddToFav);

        void updateForecastDates(List<String> forecastDates);

        void showLocationsCustomizeDialog();
    }

    interface Presenter extends IPresenter<View> {

        void onLocationPermissionsGranted();

        void onRefreshRequested();

        void onSearchButtonClick(Activity activity, int requestCode);

        void onNewGooglePlaceSelected(LocationInfo locationInfo);

        void onNavMenuItemClick(MenuItem menuItem);

        void onAddToFavButtonClick();
    }
}
