package com.anadea.svv.sunnyday.data.model.weather.adapter;

public interface WeatherInfoModel {

    String getLocationName();

    String getTemperature();

    String getWeatherDate();

    String getWeatherDateExt();

    String getWeatherTime();

    String getIconPath();

    String getMaxTemperature();

    String getMinTemperature();

    String getHumidity();

    String getWindSpeed();

    String getPressure();

    String getDescription();
}
