package com.anadea.svv.sunnyday.features.main;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.MenuItem;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.SunnyDayApp;
import com.anadea.svv.sunnyday.data.DataManager;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoAdapter;
import com.anadea.svv.sunnyday.mvp.core.BasePresenter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;

public class MainPresenter extends BasePresenter<MvpMainContract.View> implements
        MvpMainContract.Presenter {

    private enum PresenterState {NO_DATA, LOCATION_RECEIVED, WEATHER_RECEIVED}

    private static final String TAG = "MainPresenter";

    @Singleton
    @Inject
    DataManager dataManager;

    private PresenterState presenterState;

    private LocationInfo currentLocation;       // Selected Location (GPS location at start)

    MainPresenter() {
        SunnyDayApp.getAppComponent().inject(this);
        presenterState = PresenterState.NO_DATA;
    }

    /**
     * Requesting stored user's Locations from SharedPreferences
     */
    private void requestStoredUserLocations() {
        dataManager.requestAndPublishStoredUserLocations();
    }

    /**
     * Requesting stored WeatherInfo from SharedPreferences
     */
    private void requestLastWeatherInfo() {

        compositeDisposable.add(dataManager.requestLastStoredWeatherInfo()
                .subscribe(new Consumer<WeatherInfoAdapter>() {
                    @Override
                    public void accept(WeatherInfoAdapter weatherInfoAdapter) throws Exception {
                        if (!weatherInfoAdapter.isEmpty()) {
                            getView().updateCurrentWeatherData(weatherInfoAdapter);
                        }
                    }
                }));
    }

    /**
     * Requesting current user's GPS location
     */
    private void requestGpsLocation() {

        dataManager.requestCurrentUserLocation()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        compositeDisposable.add(disposable);
                        getView().startProgress();
                    }
                })
                .subscribe(new BiConsumer<LocationInfo, Throwable>() {
                    @Override
                    public void accept(LocationInfo userGpsLocationInfo, Throwable throwable) throws Exception {
                        if (throwable != null) {
                            getView().stopProgress();
                            getView().onError(throwable.getMessage());
                        } else {
                            if (!userGpsLocationInfo.isEmpty()) {
                                // Update GPS Location info in Navigation Drawer header
                                getView().updateNavHeaderLocationData(userGpsLocationInfo);
                                // Applying current location
                                setCurrentLocation(userGpsLocationInfo);
                                presenterState = PresenterState.LOCATION_RECEIVED;
                            } else {
                                getView().stopProgress();
                                getView().onError(dataManager.getContext().getString(R.string.err_no_places_found));
                            }
                        }
                    }
                });
    }

    /**
     * Requesting Current weather info and Forecast info update for current location
     */
    private void requestWeatherInfoUpdate() {
        if (currentLocation != null) {
            getView().startProgress();
            dataManager.requestAndPublishCurrentWeather(currentLocation.getLatitude(), currentLocation.getLongitude());
            dataManager.requestAndPublishFiveDaysForecast(currentLocation.getLatitude(), currentLocation.getLongitude());
        }
    }

    /**
     * Setting Current Location means update of current weather info and forecast info
     *
     * @param currentLocation: new Current Location
     */
    private void setCurrentLocation(LocationInfo currentLocation) {
        this.currentLocation = currentLocation;
        if (dataManager.isInternetAvailable())
            requestWeatherInfoUpdate();
        else
            getView().onError(dataManager.getContext().getString(R.string.err_no_internet));
    }

    /**
     * Subscription to user's locations info updates
     */
    private void subscribeToUserLocationsUpdates() {

        compositeDisposable.add(dataManager.subscribeUserLocationsConsumer(new Consumer<List<LocationInfo>>() {
            @Override
            public void accept(List<LocationInfo> locationInfoList) throws Exception {
                Collections.sort(locationInfoList);
                getView().updateNavigationDrawerMenu(locationInfoList);
            }
        }));
    }

    /**
     * Subscription to current weather info updates
     */
    private void subscribeToCurrentWeatherInfoUpdates() {

        compositeDisposable.add(dataManager.subscribeCurrentWeatherConsumer(new Consumer<WeatherInfo>() {
            @Override
            public void accept(WeatherInfo weatherInfo) throws Exception {
                getView().stopProgress();
                if (weatherInfo != null) {
                    presenterState = PresenterState.WEATHER_RECEIVED;

                    WeatherInfoAdapter weatherInfoAdapter = new WeatherInfoAdapter(currentLocation.getCityName(), weatherInfo);
                    getView().updateCurrentWeatherData(weatherInfoAdapter);
                    getView().setAddToFavButtonVisible(isAddToFavEnabled());

                    if (dataManager.getUserGpsLocationInfo().equals(currentLocation)) {
                        dataManager.saveWeatherInfo(weatherInfoAdapter);
                    }
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                getView().stopProgress();
                getView().onError(dataManager.parseErrorMessage(throwable).getMessage());
            }
        }));
    }

    /**
     * Subscription to forecast info updates
     */
    private void subscribeToForecastUpdates() {

        compositeDisposable.add(dataManager.subscribeForecastPage(new Consumer<Map<String, List<WeatherInfo>>>() {
            @Override
            public void accept(Map<String, List<WeatherInfo>> forecastMap) throws Exception {

                List<String> forecastDates = new ArrayList<>();
                for (String date : forecastMap.keySet())
                    forecastDates.add(date);

                getView().updateForecastDates(forecastDates);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                getView().onError(dataManager.parseErrorMessage(throwable).getMessage());
            }
        }));
    }

    private boolean isAddToFavEnabled() {
        return !dataManager.getUserGpsLocationInfo().equals(currentLocation)
                && !dataManager.isLocationInFavorites(currentLocation.getCityName());
    }

    @Override
    public void attachView(@NonNull MvpMainContract.View view) {
        super.attachView(view);

        subscribeToUserLocationsUpdates();
        subscribeToCurrentWeatherInfoUpdates();
        subscribeToForecastUpdates();

        switch (presenterState) {
            case NO_DATA:
                requestLastWeatherInfo();
                requestStoredUserLocations();
                getView().checkLocationPermissions();
                break;
            case LOCATION_RECEIVED:
                requestWeatherInfoUpdate();
                break;
        }
    }

    @Override
    public void onLocationPermissionsGranted() {

        if (dataManager.isInternetAvailable())
            requestGpsLocation();
        else {
            getView().onError(dataManager.getContext().getString(R.string.err_no_internet));
            getView().stopProgress();
        }
    }

    @Override
    public void onRefreshRequested() {

        if (presenterState == PresenterState.LOCATION_RECEIVED
                || presenterState == PresenterState.WEATHER_RECEIVED) {
            requestWeatherInfoUpdate();
        } else {
            requestStoredUserLocations();
            getView().checkLocationPermissions();
        }
    }

    @Override
    public void onSearchButtonClick(Activity activity, int requestCode) {

        try {
            Intent intent = new PlaceAutocomplete
                    .IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(activity);
            activity.startActivityForResult(intent, requestCode);
        } catch (GooglePlayServicesRepairableException e) {
            getView().onError(String.format(Locale.getDefault(), "%s: %s",
                    dataManager.getContext().getString(R.string.err_google_services_error),
                    e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            getView().onError(String.format(Locale.getDefault(), "%s: %s",
                    dataManager.getContext().getString(R.string.err_google_services_na),
                    e.getMessage()));
        }
    }

    @Override
    public void onNewGooglePlaceSelected(LocationInfo locationInfo) {
        setCurrentLocation(locationInfo);
    }

    @Override
    public void onNavMenuItemClick(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case MainActivity.MENU_ITEM_ID_AUTODETECT:
                getView().checkLocationPermissions();
                break;
            case MainActivity.MENU_ITEM_ID_CUSTOMIZE:
                getView().showLocationsCustomizeDialog();
                break;
            default:
                LocationInfo locationInfo = dataManager.getStoredLocationInfoByName(menuItem.getTitle().toString());
                if (locationInfo != null) {
                    setCurrentLocation(locationInfo);
                }
                break;
        }
    }

    @Override
    public void onAddToFavButtonClick() {
        dataManager.addLocationToFavorites(currentLocation)
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        getView().setAddToFavButtonVisible(isAddToFavEnabled());
                    }
                });
    }

}
