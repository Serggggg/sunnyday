package com.anadea.svv.sunnyday.mvp;

import com.anadea.svv.sunnyday.mvp.core.BaseActivity;
import com.anadea.svv.sunnyday.mvp.core.IPresenter;
import com.anadea.svv.sunnyday.mvp.core.IView;

public abstract class MvpActivity<V extends IView, P extends IPresenter<V>> extends BaseActivity<P> {

    @Override
    protected void onStart() {
        super.onStart();

        //noinspection unchecked
        mvpComponent.attachView((V) this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mvpComponent.detachView();
    }

    @Override
    protected void notifyComponentOnDestroy(boolean isChangingConfigurations) {

        if (!isChangingConfigurations)
            mvpComponent.destroy();
    }

    protected P getPresenter() {
        return mvpComponent;
    }
}
