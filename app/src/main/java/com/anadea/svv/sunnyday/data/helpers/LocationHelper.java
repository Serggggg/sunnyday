package com.anadea.svv.sunnyday.data.helpers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;

import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LocationHelper {

    private static final String TAG = "LocationHelper";

    public LocationHelper() {
    }

    private GoogleApiClient obtainGoogleApiClient(@NonNull Context context,
                                                  @NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {

        return new GoogleApiClient
                .Builder(context)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(onConnectionFailedListener)
                .build();
    }

    /**
     * Requesting current GPS location via Google Play Services
     *
     * @param context: app context
     * @return Single source with recognized LocationInfo
     */
    public Single<LocationInfo> requestCurrentPlace(final @NonNull Context context) {

        return Single.create(new SingleOnSubscribe<LocationInfo>() {
            @Override
            public void subscribe(@NonNull final SingleEmitter<LocationInfo> emitter) throws Exception {

                final GoogleApiClient googleApiClient = obtainGoogleApiClient(context,
                        new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                emitter.onError(new Throwable(connectionResult.getErrorMessage()));
                            }
                        });
                googleApiClient.connect();

                // Permissions are checked and requested by Presenter
                //noinspection MissingPermission
                PendingResult<PlaceLikelihoodBuffer> result = Places
                        .PlaceDetectionApi
                        .getCurrentPlace(googleApiClient, null);
                result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {

                        handleLikelyPlaces(placeLikelihoods,
                                new Geocoder(googleApiClient.getContext(), Locale.getDefault()))
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.io())
                                .subscribe(new Consumer<LocationInfo>() {
                                    @Override
                                    public void accept(LocationInfo locationInfo) throws Exception {
                                        emitter.onSuccess(locationInfo);
                                    }
                                });
                    }
                });
            }
        });
    }

    /**
     * Handling likely places received via PlaceDetectionApi.getCurrentPlace()
     * Method selects most likely place and creates a LocationInfo object via Geocoder
     *
     * @param likelyPlaces: PlaceLikelihoodBuffer with likely places
     * @param geocoder: Geocoder instance
     * @return Single source with LocationInfo
     */
    private Single<LocationInfo> handleLikelyPlaces(final @NonNull PlaceLikelihoodBuffer likelyPlaces,
                                                    final @NonNull Geocoder geocoder) {

        return Single.fromCallable(new Callable<LocationInfo>() {
            @Override
            public LocationInfo call() throws Exception {

                // Getting most likely place
                LocationInfo recognizedLocationInfo = LocationInfo.createEmpty();

                Place mostLikelyPlace = null;
                float mostLikelihood = -1f;
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    if (placeLikelihood.getLikelihood() > mostLikelihood) {
                        mostLikelihood = placeLikelihood.getLikelihood();
                        mostLikelyPlace = placeLikelihood.getPlace();
                    }
                }

                // Transform most likely place to LocationInfo with GeoCoder
                if (mostLikelyPlace != null) {
                    if (Geocoder.isPresent())
                        try {
                            List<Address> addressList = geocoder
                                    .getFromLocation(mostLikelyPlace.getLatLng().latitude, mostLikelyPlace.getLatLng().longitude, 5);
                            if (!addressList.isEmpty()) {
                                Address address = addressList.get(0);
                                recognizedLocationInfo =
                                        new LocationInfo(address.getLocality(), address.getCountryName(),
                                                address.getLatitude(), address.getLongitude());
                            }
                        } catch (IOException e) {
                            throw new Exception("GeoCoder error: " + e.getMessage());
                        } finally {
                            likelyPlaces.release();
                        }
                }

                return recognizedLocationInfo;
            }
        });
    }

}
