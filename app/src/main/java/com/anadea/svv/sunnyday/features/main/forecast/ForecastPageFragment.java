package com.anadea.svv.sunnyday.features.main.forecast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoAdapter;
import com.anadea.svv.sunnyday.mvp.MvpFragment;

import java.util.List;

public class ForecastPageFragment extends MvpFragment<MvpForecastContract.View, MvpForecastContract.Presenter>
        implements MvpForecastContract.View {

    private static final String TAG = "ForecastPageFragment";

    private static final String KEY_PAGE_NUM = "page_num";

    private ForecastContentAdapter contentAdapter;

    public static ForecastPageFragment newInstance(int pageNum) {

        Bundle args = new Bundle();
        args.putInt(KEY_PAGE_NUM, pageNum);
        ForecastPageFragment fragment = new ForecastPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected MvpForecastContract.Presenter createMvpComponent() {
        return new ForecastPagePresenter(getArguments().getInt(KEY_PAGE_NUM, 0));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forecast_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView contentRecyclerView = view.findViewById(R.id.forecast_recyclerView);
        contentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contentRecyclerView.setHasFixedSize(true);
        contentRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        contentAdapter = new ForecastContentAdapter();
        contentRecyclerView.setAdapter(contentAdapter);
    }

    @Override
    public void updateForecastData(List<WeatherInfo> weatherInfoList) {

        contentAdapter.updateData(WeatherInfoAdapter.transformWeatherInfoList(null, weatherInfoList));
    }
}
