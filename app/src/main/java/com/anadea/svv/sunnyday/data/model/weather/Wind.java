package com.anadea.svv.sunnyday.data.model.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    private float speed;

    @SerializedName("deg")
    @Expose
    private float degree;

    public float getSpeed() {
        return speed;
    }

    public float getDegree() {
        return degree;
    }
}
