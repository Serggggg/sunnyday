package com.anadea.svv.sunnyday.data.remote;

import com.anadea.svv.sunnyday.data.model.forecast.ForecastInfo;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface OpenWeatherApi {

    @GET("weather")
    Single<WeatherInfo> getCurrentWeather(@Query("lat") double latitude, @Query("lon") double longitude);

    @GET("weather")
    Single<WeatherInfo> getCurrentWeatherByName(@Query("q") String name);

    @GET("forecast")
    Single<ForecastInfo> getFiveDaysForecast(@Query("lat") double latitude, @Query("lon") double longitude);
}
