package com.anadea.svv.sunnyday.di.modules;

import com.anadea.svv.sunnyday.data.helpers.LocationHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class LocationModule {

    @Provides
    LocationHelper provideLocationHelper() {
        return new LocationHelper();
    }
}
