package com.anadea.svv.sunnyday.di.modules;

import com.anadea.svv.sunnyday.data.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Singleton
    @Provides
    DataManager provideDataManager() {
        return new DataManager();
    }
}
