package com.anadea.svv.sunnyday.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class IntegerPreference extends BasePreference<Integer> {

    public static final int DEFAULT_VALUE = 0;

    public IntegerPreference(@NonNull SharedPreferences preferences, @NonNull String key) {
        this(preferences, key, DEFAULT_VALUE);
    }

    public IntegerPreference(@NonNull SharedPreferences preferences, @NonNull String key,
                             @NonNull Integer defaultValue) {
        super(preferences, key, defaultValue);
    }

    @Override
    public Integer get() {
        return preferences.getInt(key, defaultValue);
    }

    @Override
    public void set(@NonNull Integer value) {
        preferences.edit().putInt(key, value).apply();
    }
}
