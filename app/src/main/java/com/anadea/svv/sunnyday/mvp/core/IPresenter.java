package com.anadea.svv.sunnyday.mvp.core;

import android.support.annotation.NonNull;

public interface IPresenter<V extends IView> {

    void attachView(@NonNull V view);

    void detachView();

    void destroy();
}
