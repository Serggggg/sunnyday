package com.anadea.svv.sunnyday.features.main.forecast;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoAdapter;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ForecastContentAdapter extends RecyclerView.Adapter<ForecastItemViewHolder> {

    private List<WeatherInfoAdapter> weatherInfoAdapterList = new ArrayList<>();

    @Override
    public ForecastItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_list_item, parent, false);
        return new ForecastItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ForecastItemViewHolder holder, int position) {

        WeatherInfoModel weatherInfoModel = weatherInfoAdapterList.get(position);

        Picasso.with(holder.itemView.getContext())
                .load(weatherInfoModel.getIconPath())
                .into(holder.iconImageView);
        holder.tempTextView.setText(weatherInfoModel.getTemperature());
        holder.timeTextView.setText(weatherInfoModel.getWeatherTime());
        holder.descriptionTextView.setText(weatherInfoModel.getDescription());
        holder.pressureTextView.setText(weatherInfoModel.getPressure());
        holder.windTextView.setText(weatherInfoModel.getWindSpeed());
        holder.humidityTextView.setText(weatherInfoModel.getHumidity());
    }

    @Override
    public int getItemCount() {
        return weatherInfoAdapterList.size();
    }

    public void updateData(List<WeatherInfoAdapter> weatherInfoList) {
        this.weatherInfoAdapterList = weatherInfoList;
        notifyDataSetChanged();
    }
}
