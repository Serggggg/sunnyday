package com.anadea.svv.sunnyday.mvp.core;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<V extends IView> implements IPresenter<V> {

    private V mvpView;

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void attachView(@NonNull V view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        compositeDisposable.clear();
        mvpView = null;
    }

    @Override
    public void destroy() {
    }

    protected final V getView() {
        return mvpView;
    }

    protected final boolean isViewAttached() {
        return mvpView != null;
    }
}
