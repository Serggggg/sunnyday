package com.anadea.svv.sunnyday.features.customize_dialog;

import android.support.annotation.NonNull;
import android.view.View;

import com.anadea.svv.sunnyday.SunnyDayApp;
import com.anadea.svv.sunnyday.data.DataManager;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.mvp.core.BasePresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class LocationsCustomizePresenter extends BasePresenter<MvpCustomizeDialogContract.View>
        implements MvpCustomizeDialogContract.Presenter,
        LocationListItemViewHolder.ItemClickListener {

    private static final String TAG = "LocCustomizePresenter";

    @Singleton
    @Inject
    DataManager dataManager;

    LocationsCustomizePresenter() {
        SunnyDayApp.getAppComponent().inject(this);
    }

    /**
     * Subscription to user's locations info updates
     */
    private void subscribeToUserLocationsUpdates() {

        compositeDisposable.add(dataManager.subscribeUserLocationsConsumer(new Consumer<List<LocationInfo>>() {
            @Override
            public void accept(List<LocationInfo> locationInfoList) throws Exception {
                getView().updateLocationsListData(locationInfoList);
            }
        }));
    }

    @Override
    public void attachView(@NonNull MvpCustomizeDialogContract.View view) {
        super.attachView(view);
        subscribeToUserLocationsUpdates();
    }

    @Override
    public void onListItemClick(View view, int position) {
        getView().markListItem(position);
    }

    @Override
    public void onCloseButtonClick() {
        getView().closeDialog();
    }

    @Override
    public void onRemoveButtonClick() {
        Set<LocationInfo> locationsToRemoveSet = getView().getSelectedLocations();
        if (locationsToRemoveSet.size() == 0) {
            getView().closeDialog();
            return;
        }
        List<LocationInfo> locationsToRemoveList = new ArrayList<>();
        for (LocationInfo locationInfo : locationsToRemoveSet)
            locationsToRemoveList.add(locationInfo);
        dataManager.removeLocationsFromFavorites(locationsToRemoveList)
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        getView().closeDialog();
                    }
                });
    }

    @Override
    public void onClearButtonClick() {
        getView().clearSelection();
    }
}
