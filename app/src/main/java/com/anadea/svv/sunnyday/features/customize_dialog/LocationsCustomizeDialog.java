package com.anadea.svv.sunnyday.features.customize_dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.mvp.MvpDialogFragment;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LocationsCustomizeDialog extends MvpDialogFragment<MvpCustomizeDialogContract.View, MvpCustomizeDialogContract.Presenter>
        implements MvpCustomizeDialogContract.View {

    private static final String TAG = "LocationsCustomizeDlg";

    private Unbinder unbinder;

    private LocationsDialogContentAdapter contentAdapter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_dialog_locations_customize, null);

        unbinder = ButterKnife.bind(this, rootView);

        initControls(rootView);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(rootView);
        return builder.create();
    }

    private void initControls(View root) {

        TextView title = root.findViewById(R.id.custom_dialog_title_textView);
        title.setText(R.string.dialog_title_customize_locations);

        RecyclerView recyclerView = root.findViewById(R.id.locations_dialog_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        if (getPresenter() instanceof LocationListItemViewHolder.ItemClickListener) {
            LocationListItemViewHolder.ItemClickListener listener = (LocationListItemViewHolder.ItemClickListener) getPresenter();
            contentAdapter = new LocationsDialogContentAdapter(getContext(), new WeakReference<>(listener));
            recyclerView.setAdapter(contentAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected MvpCustomizeDialogContract.Presenter createMvpComponent() {
        return new LocationsCustomizePresenter();
    }

    @OnClick(R.id.custom_dialog_close_button)
    void onCloseButtonClick() {
        getPresenter().onCloseButtonClick();
    }

    @OnClick(R.id.locations_dialog_save_button)
    void onSaveButtonClick() {
        getPresenter().onRemoveButtonClick();
    }

    @OnClick(R.id.locations_dialog_clean_button)
    void onClearButtonClick() {
        getPresenter().onClearButtonClick();
    }

    @Override
    public void updateLocationsListData(List<LocationInfo> locationInfoList) {
        contentAdapter.updateDataSet(locationInfoList);
    }

    @Override
    public void markListItem(int position) {
        contentAdapter.markItem(position);
    }

    @Override
    public void clearSelection() {
        contentAdapter.clearSelection();
    }

    @Override
    public Set<LocationInfo> getSelectedLocations() {
        return contentAdapter.getSelectedLocations();
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

}
