package com.anadea.svv.sunnyday.features.customize_dialog;

import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.mvp.core.IPresenter;
import com.anadea.svv.sunnyday.mvp.core.IView;

import java.util.List;
import java.util.Set;

interface MvpCustomizeDialogContract {

    interface View extends IView {

        void updateLocationsListData(List<LocationInfo> locationInfoList);

        void markListItem(int position);

        void clearSelection();

        Set<LocationInfo> getSelectedLocations();

        void closeDialog();
    }

    interface Presenter extends IPresenter<View> {

        void onCloseButtonClick();

        void onRemoveButtonClick();

        void onClearButtonClick();

    }
}
