package com.anadea.svv.sunnyday.features.customize_dialog;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LocationsDialogContentAdapter extends RecyclerView.Adapter<LocationListItemViewHolder> {

    private List<LocationInfo> locationInfoList = new ArrayList<>();
    private Set<LocationInfo> selectedLocations = new HashSet<>();

    private WeakReference<LocationListItemViewHolder.ItemClickListener> itemClickListenerReference;

    private int bkgColorNormal;
    private int bkgColorSelected;

    public LocationsDialogContentAdapter(Context context,
                                         WeakReference<LocationListItemViewHolder.ItemClickListener> itemClickListenerReference) {
        this.itemClickListenerReference = itemClickListenerReference;

        bkgColorNormal = ContextCompat.getColor(context, R.color.white);
        bkgColorSelected = ContextCompat.getColor(context, R.color.white_smoke);
    }

    @Override
    public LocationListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_item, parent, false);
        return new LocationListItemViewHolder(itemView, itemClickListenerReference);
    }

    @Override
    public void onBindViewHolder(LocationListItemViewHolder holder, int position) {

        LocationInfo locationInfo = locationInfoList.get(position);
        holder.titleTextView.setText(locationInfo.getCityName());

        boolean selected = selectedLocations.contains(locationInfo);
        holder.itemView.setBackgroundColor(selected ? bkgColorSelected : bkgColorNormal);
        holder.checkedImageView.setImageResource(selected
                ? R.drawable.ic_delete_24dp : R.drawable.ic_checkbox_unchecked_24dp);
    }

    @Override
    public int getItemCount() {
        return locationInfoList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        itemClickListenerReference = null;
    }

    public void updateDataSet(List<LocationInfo> locationInfoList) {
        this.locationInfoList = locationInfoList;
        notifyDataSetChanged();
    }

    public void markItem(int position) {

        LocationInfo locationInfo = locationInfoList.get(position);
        if (selectedLocations.contains(locationInfo))
            selectedLocations.remove(locationInfo);
        else
            selectedLocations.add(locationInfo);

        notifyItemChanged(position);
    }

    public void clearSelection() {
        selectedLocations.clear();
        notifyDataSetChanged();
    }

    public Set<LocationInfo> getSelectedLocations() {
        return selectedLocations;
    }

}
