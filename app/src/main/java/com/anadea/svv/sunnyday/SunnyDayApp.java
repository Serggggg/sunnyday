package com.anadea.svv.sunnyday;

import android.app.Application;

import com.anadea.svv.sunnyday.di.components.AppComponent;
import com.anadea.svv.sunnyday.di.components.DaggerAppComponent;
import com.anadea.svv.sunnyday.di.modules.AppModule;

public class SunnyDayApp extends Application {

    private static final String TAG = "SunnyDayApp";

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

}
