package com.anadea.svv.sunnyday.features.main.forecast;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.anadea.svv.sunnyday.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.forecast_list_item_time_textView) TextView timeTextView;
    @BindView(R.id.forecast_list_item_icon_imageView) ImageView iconImageView;
    @BindView(R.id.forecast_list_item_description_textView) TextView descriptionTextView;
    @BindView(R.id.forecast_list_item_temp_textView) TextView tempTextView;
    @BindView(R.id.forecast_list_item_pressure_textView) TextView pressureTextView;
    @BindView(R.id.forecast_list_item_wind_textView) TextView windTextView;
    @BindView(R.id.forecast_list_item_humidity_textView) TextView humidityTextView;

    public ForecastItemViewHolder(View itemView) {

        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
