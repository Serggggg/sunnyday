package com.anadea.svv.sunnyday.data.model.forecast;

import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ForecastInfo {

    @SerializedName("list")
    @Expose
    private final List<WeatherInfo> weatherInfoList = new ArrayList<>();

    public List<WeatherInfo> getWeatherInfoList() {
        return weatherInfoList;
    }
}
