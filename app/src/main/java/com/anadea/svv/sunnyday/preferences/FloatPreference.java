package com.anadea.svv.sunnyday.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class FloatPreference extends BasePreference<Float> {

    public static final float DEFAULT_VALUE = 0f;

    public FloatPreference(@NonNull SharedPreferences preferences, @NonNull String key) {
        this(preferences, key, DEFAULT_VALUE);
    }

    public FloatPreference(@NonNull SharedPreferences preferences, @NonNull String key,
                           @NonNull Float defaultValue) {
        super(preferences, key, defaultValue);
    }

    @Override
    public Float get() {
        return preferences.getFloat(key, defaultValue);
    }

    @Override
    public void set(@NonNull Float value) {
        preferences.edit().putFloat(key, value).apply();
    }
}
