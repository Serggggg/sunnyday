package com.anadea.svv.sunnyday.data.model.errors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorMessage {

    @SerializedName("cod")
    @Expose
    private final int errCode;
    @SerializedName("message")
    @Expose
    private final String message;

    public ErrorMessage(int errCode, String message) {
        this.errCode = errCode;
        this.message = message;
    }

    public int getErrCode() {
        return errCode;
    }

    public String getMessage() {
        return message;
    }
}
