package com.anadea.svv.sunnyday.features.main.forecast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ForecastPagerAdapter extends FragmentPagerAdapter {

    private List<String> forecastDates = new ArrayList<>();

    public ForecastPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    public void updatePageTitles(List<String> pageTitles) {
        forecastDates = pageTitles;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return ForecastPageFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return forecastDates.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return forecastDates.get(position);
    }
}
