package com.anadea.svv.sunnyday.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.SunnyDayApp;
import com.anadea.svv.sunnyday.data.helpers.LocationHelper;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.data.model.forecast.ForecastInfo;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.anadea.svv.sunnyday.data.model.errors.ErrorMessage;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoAdapter;
import com.anadea.svv.sunnyday.data.remote.RestService;
import com.anadea.svv.sunnyday.data.helpers.PreferencesHelper;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class DataManager {

   private static final String TAG = "DataManager";

    @Inject
    Context context;

    @Inject
    RestService restService;

    @Inject
    PreferencesHelper preferencesHelper;

    @Inject
    LocationHelper locationHelper;

    // Places and locations from Preferences
    private final Map<String, LocationInfo> userLocationsMap = new LinkedHashMap<>();
    // User's current GPS location
    private LocationInfo userGpsLocationInfo;

    // Stored User locations publisher
    private final Subject<List<LocationInfo>> userLocationsBehaviorSubject = BehaviorSubject.create();
    // Current weather info publisher
    private final Subject<WeatherInfo> currentWeatherSubject = BehaviorSubject.create();
    // Forecast info publisher
    private final Subject<Map<String, List<WeatherInfo>>> forecastBehaviorSubject = BehaviorSubject.create();

    public DataManager() {
        SunnyDayApp.getAppComponent().inject(this);
    }

    public Context getContext() {
        return context;
    }

    public boolean isInternetAvailable() {
        NetworkInfo info = ((ConnectivityManager)(context
                .getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

    public List<LocationInfo> getStoredLocationsList() {
        List<LocationInfo> storedLocations = new ArrayList<>();
        for (Map.Entry<String, LocationInfo> entry : userLocationsMap.entrySet())
            storedLocations.add(entry.getValue());
        return storedLocations;
    }

    public LocationInfo getStoredLocationInfoByName(String locationName) {
        return userLocationsMap.containsKey(locationName)
                ? userLocationsMap.get(locationName)
                : null;
    }

    public boolean isLocationInFavorites(String locationName) {
        return userLocationsMap.containsKey(locationName);
    }

    public LocationInfo getUserGpsLocationInfo() {
        return userGpsLocationInfo;
    }

    /**
     * Subscribing a new consumer to stored user's locations info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    public Disposable subscribeUserLocationsConsumer(@NonNull Consumer<List<LocationInfo>> consumer) {
        return userLocationsBehaviorSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer);
    }

    /**
     * Subscribing a new consumer to current weather info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    public Disposable subscribeCurrentWeatherConsumer(@NonNull Consumer<WeatherInfo> consumer,
                                                      @NonNull Consumer<Throwable> throwableConsumer) {
        return currentWeatherSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, throwableConsumer);
    }

    /**
     * Subscribing a new consumer to forecast info update
     *
     * @param consumer: Consumer
     * @return Disposable
     */
    public Disposable subscribeForecastPage(@NonNull Consumer<Map<String, List<WeatherInfo>>> consumer,
                                            @NonNull Consumer<Throwable> throwableConsumer) {
        return forecastBehaviorSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, throwableConsumer);
    }

    /**
     * Requesting and publishing user's places and locations from SharedPreferences
     * into userLocationsBehaviorSubject
     */
    public void requestAndPublishStoredUserLocations() {

        preferencesHelper.readStoredUserLocations()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Consumer<List<LocationInfo>>() {
                    @Override
                    public void accept(List<LocationInfo> locationInfoList) throws Exception {
                        userLocationsMap.clear();
                        for (LocationInfo locationInfo : locationInfoList) {
                            userLocationsMap.put(locationInfo.getCityName(), locationInfo);
                        }
                        userLocationsBehaviorSubject.onNext(locationInfoList);
                    }
                });
    }

    /**
     * Add new location info to stored user locations and save list to SharedPreferences
     *
     * @param locationInfo: new Location info
     */
    public Completable addLocationToFavorites(final LocationInfo locationInfo) {

        final List<LocationInfo> storedLocations = new ArrayList<>();
        for (Map.Entry<String, LocationInfo> entry : userLocationsMap.entrySet())
            storedLocations.add(entry.getValue());
        storedLocations.add(locationInfo);

        return preferencesHelper.saveStoredUserLocations(storedLocations)
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        userLocationsMap.put(locationInfo.getCityName(), locationInfo);
                        userLocationsBehaviorSubject.onNext(storedLocations);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Remove locations from stored user locations and save list to SharedPreferences
     *
     * @param removeList: List of locations to remove
     */
    public Completable removeLocationsFromFavorites(final List<LocationInfo> removeList) {

        for (LocationInfo locationInfo : removeList)
            userLocationsMap.remove(locationInfo.getCityName());

        final List<LocationInfo> storedLocations = new ArrayList<>();
        for (Map.Entry<String, LocationInfo> entry : userLocationsMap.entrySet())
            storedLocations.add(entry.getValue());

        return preferencesHelper.saveStoredUserLocations(storedLocations)
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        userLocationsBehaviorSubject.onNext(storedLocations);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Requesting stored weather info for last known user's GPS location from SharedPreferences
     *
     * @return Single source with WeatherInfoAdapter (includes location name)
     */
    public Single<WeatherInfoAdapter> requestLastStoredWeatherInfo() {

        return preferencesHelper.readLastWeatherInfoFromPrefs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Saving weather info for last known user's GPS location to SharedPreferences
     *
     * @param weatherInfoAdapter: WeatherInfoAdapter (includes location name)
     */
    public void saveWeatherInfo(WeatherInfoAdapter weatherInfoAdapter) {
        preferencesHelper.saveLastWeatherInfoToPrefs(weatherInfoAdapter);
    }

    /**
     * Requesting current user's GPS location
     *
     * @return Single source with LocationInfo received via GooglePlaces API
     */
    public Single<LocationInfo> requestCurrentUserLocation() {

        return locationHelper.requestCurrentPlace(context)
                .observeOn(Schedulers.io())
                .doOnSuccess(new Consumer<LocationInfo>() {
                    @Override
                    public void accept(LocationInfo locationInfo) throws Exception {
                        if (!locationInfo.isEmpty())
                            userGpsLocationInfo = locationInfo;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Requesting and publishing current weather into currentWeatherSubject
     *
     * @param latitude:  location's latitude
     * @param longitude: location's longitude
     */
    public void requestAndPublishCurrentWeather(double latitude, double longitude) {

        restService.requestCurrentWeather(latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BiConsumer<WeatherInfo, Throwable>() {
                    @Override
                    public void accept(WeatherInfo weatherInfo, Throwable throwable) throws Exception {
                        if (throwable == null)
                            currentWeatherSubject.onNext(weatherInfo);
                        else
                            currentWeatherSubject.onError(throwable);
                    }
                });
    }

    /**
     * Requesting and publishing 5 day forecast includes weather data every 3 hours info
     * into forecastBehaviorSubject
     *
     * @param latitude:  location latitude
     * @param longitude: location longitude
     */
    public void requestAndPublishFiveDaysForecast(double latitude, double longitude) {

        restService.requestFiveDaysForecast(latitude, longitude)
                .map(new Function<ForecastInfo, Map<String, List<WeatherInfo>>>() {
                    @Override
                    public Map<String, List<WeatherInfo>> apply(@NonNull ForecastInfo forecastInfo) throws Exception {

                        // Transform forecast WeatherInfo list into LinkedHashMap
                        // (KEY = date, VALUE = List of WeatherInfo)
                        SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

                        Map<String, List<WeatherInfo>> forecastMap = new LinkedHashMap<>();
                        for (int i = 0; i < forecastInfo.getWeatherInfoList().size(); i++) {
                            WeatherInfo weatherInfo = forecastInfo.getWeatherInfoList().get(i);

                            String forecastDate = TIME_FORMAT.format(new Date(weatherInfo.getTimestamp() * 1000));
                            if (!forecastMap.containsKey(forecastDate))
                                forecastMap.put(forecastDate, new ArrayList<WeatherInfo>());
                            forecastMap.get(forecastDate).add(weatherInfo);
                        }

                        return forecastMap;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new BiConsumer<Map<String, List<WeatherInfo>>, Throwable>() {
                    @Override
                    public void accept(Map<String, List<WeatherInfo>> forecastMap, Throwable throwable) throws Exception {
                        if (throwable == null)
                            forecastBehaviorSubject.onNext(forecastMap);
                        else
                            forecastBehaviorSubject.onError(throwable);
                    }
                });
    }

    public ErrorMessage parseErrorMessage(@NonNull Throwable throwable) {

        if (throwable instanceof HttpException) {
            ResponseBody body = ((HttpException) throwable).response().errorBody();
            if (body != null) {
                try {
                    String errorString = body.string();
                    return new Gson().fromJson(errorString, ErrorMessage.class);
                }
                catch (IOException e) {
                    Log.e(TAG, "Error message parse failed: " + e.getMessage());
                }
            }
        }

        if (throwable instanceof IOException) {
            String errorMessage = String.format(Locale.getDefault(), "%s: %s",
                    context.getString(R.string.err_network_error),
                    throwable.getMessage());
            return new ErrorMessage(0, errorMessage);
        }

        String errorMessage = String.format(Locale.getDefault(), "%s: %s",
                context.getString(R.string.err_unexpected_error),
                throwable.getMessage());
        return new ErrorMessage(0, errorMessage);
    }

    /**
     * Requesting current weather from REST service
     *
     * @param latitude:  location's latitude
     * @param longitude: location's longitude
     * @return Single source with WeatherInfo for appropriate coordinates
     */
    @Deprecated
    public Single<WeatherInfo> requestCurrentWeather(double latitude, double longitude) {

        return restService.requestCurrentWeather(latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Requesting 5 day forecast includes weather data every 3 hours from REST service
     *
     * @param latitude:  location's latitude
     * @param longitude: location's longitude
     * @return Single source with forecast LinkedHashMap (KEY = date, VALUE = List of WeatherInfo)
     */
    @Deprecated
    public Single<Map<String, List<WeatherInfo>>> requestFiveDaysForecast(double latitude, double longitude) {

        return restService.requestFiveDaysForecast(latitude, longitude)
                .map(new Function<ForecastInfo, Map<String, List<WeatherInfo>>>() {
                    @Override
                    public Map<String, List<WeatherInfo>> apply(@NonNull ForecastInfo forecastInfo) throws Exception {

                        SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

                        Map<String, List<WeatherInfo>> forecastMap = new LinkedHashMap<>();
                        for (int i = 0; i < forecastInfo.getWeatherInfoList().size(); i++) {
                            WeatherInfo weatherInfo = forecastInfo.getWeatherInfoList().get(i);

                            String forecastDate = TIME_FORMAT.format(new Date(weatherInfo.getTimestamp() * 1000));
                            if (!forecastMap.containsKey(forecastDate))
                                forecastMap.put(forecastDate, new ArrayList<WeatherInfo>());
                            forecastMap.get(forecastDate).add(weatherInfo);
                        }

                        return forecastMap;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
