package com.anadea.svv.sunnyday.features.main.forecast;

import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.anadea.svv.sunnyday.mvp.core.IPresenter;
import com.anadea.svv.sunnyday.mvp.core.IView;

import java.util.List;

public interface MvpForecastContract {

    interface View extends IView {

        void updateForecastData(List<WeatherInfo> weatherInfoList);
    }

    interface Presenter extends IPresenter<View> {

    }
}
