package com.anadea.svv.sunnyday.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.anadea.svv.sunnyday.mvp.core.IPresenter;
import com.anadea.svv.sunnyday.mvp.core.IView;

public abstract class MvpFragment<V extends IView, P extends IPresenter<V>> extends Fragment {

    private P mvpComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpComponent = createMvpComponent();
    }

    @Override
    public void onResume() {
        super.onResume();
        //noinspection unchecked
        mvpComponent.attachView((V) this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mvpComponent.detachView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mvpComponent.destroy();
        mvpComponent = null;
    }

    protected P getPresenter() {
        return mvpComponent;
    }

    protected abstract P createMvpComponent();
}
