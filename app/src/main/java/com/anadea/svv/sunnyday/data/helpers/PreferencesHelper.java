package com.anadea.svv.sunnyday.data.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoAdapter;
import com.anadea.svv.sunnyday.preferences.StringPreference;
import com.anadea.svv.sunnyday.preferences.StringSetPreference;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

public class PreferencesHelper {

    private static final String TAG = "PreferencesHelper";

    private static final String PREFERENCES_FILE_NAME = "preferences";
    private static final String PREFERENCE_NAME_USER_LOCATIONS = "user_locations";
    private static final String PREFERENCE_NAME_LAST_WEATHER_INFO = "last_weather_info";

    private final Context context;

    public PreferencesHelper(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Read list of stored (favorites) user's locations from SharedPreferences
     *
     * @return Single source with list of LocationInfo
     */
    public Single<List<LocationInfo>> readStoredUserLocations() {

        return Single.fromCallable(new Callable<List<LocationInfo>>() {
            @Override
            public List<LocationInfo> call() throws Exception {

                StringSetPreference userLocationsPreference = new StringSetPreference(getSharedPreferences(),
                        PREFERENCE_NAME_USER_LOCATIONS);

                List<LocationInfo> result = new ArrayList<>();

                Set<String> userLocationsStringSet = userLocationsPreference.isSet()
                        ? userLocationsPreference.get()
                        : new LinkedHashSet<String>();

                for (String locationString : userLocationsStringSet) {
                    result.add(new Gson().fromJson(locationString, LocationInfo.class));
                }

                return result;
            }
        });
    }

    /**
     * Saving list of stored (favorites) user's locations to SharedPreferences
     *
     * @param locationInfoList: list of LocationInfo to save
     * @return Completable source
     */
    public Completable saveStoredUserLocations(final List<LocationInfo> locationInfoList) {

        return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {

                StringSetPreference userLocationsPreference = new StringSetPreference(getSharedPreferences(),
                        PREFERENCE_NAME_USER_LOCATIONS);

                Set<String> userLocationsStringSet = new LinkedHashSet<>();
                for (LocationInfo locationInfo : locationInfoList)
                    userLocationsStringSet.add(new Gson().toJson(locationInfo));

                userLocationsPreference.set(userLocationsStringSet);
            }
        });
    }

    /**
     * Read stored weather info for last known user's GPS location from SharedPreferences
     *
     * @return Single source with WeatherInfoAdapter (includes location name)
     */
    public Single<WeatherInfoAdapter> readLastWeatherInfoFromPrefs() {

        return Single.fromCallable(new Callable<WeatherInfoAdapter>() {
            @Override
            public WeatherInfoAdapter call() throws Exception {

                StringPreference lastWeatherInfoPreference =
                        new StringPreference(getSharedPreferences(), PREFERENCE_NAME_LAST_WEATHER_INFO);

                WeatherInfoAdapter lastWeatherInfo = new WeatherInfoAdapter(null, null);

                if (lastWeatherInfoPreference.isSet())
                    lastWeatherInfo = new Gson().fromJson(lastWeatherInfoPreference.get(), WeatherInfoAdapter.class);

                return lastWeatherInfo;
            }
        });
    }

    /**
     * Saving weather info for last known user's GPS location to SharedPreferences
     */
    public void saveLastWeatherInfoToPrefs(WeatherInfoAdapter weatherInfoAdapter) {

        new StringPreference(getSharedPreferences(), PREFERENCE_NAME_LAST_WEATHER_INFO)
                .set(new Gson().toJson(weatherInfoAdapter));
    }

}
