package com.anadea.svv.sunnyday.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.util.Set;

public class StringSetPreference extends BasePreference<Set<String>> {

    public StringSetPreference(@NonNull SharedPreferences preferences, @NonNull String key) {
        this(preferences, key, null);
    }

    public StringSetPreference(@NonNull SharedPreferences preferences, @NonNull String key,
                               Set<String> defaultValue) {
        super(preferences, key, defaultValue);
    }

    @Override
    public Set<String> get() {
        return preferences.getStringSet(key, defaultValue);
    }

    @Override
    public void set(@NonNull Set<String> value) {
        preferences.edit().putStringSet(key, value).apply();
    }
}
