package com.anadea.svv.sunnyday.di.modules;

import com.anadea.svv.sunnyday.data.remote.RestService;

import dagger.Module;
import dagger.Provides;

@Module
public class RestModule {

    @Provides
    RestService provideRestService() {
        return new RestService();
    }
}
