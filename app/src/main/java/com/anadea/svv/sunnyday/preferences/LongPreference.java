package com.anadea.svv.sunnyday.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class LongPreference extends BasePreference<Long> {

    public static final long DEFAULT_VALUE = 0L;

    public LongPreference(@NonNull SharedPreferences preferences, @NonNull String key) {
        this(preferences, key, DEFAULT_VALUE);
    }

    public LongPreference(@NonNull SharedPreferences preferences, @NonNull String key,
                          @NonNull Long defaultValue) {
        super(preferences, key, defaultValue);
    }

    @Override
    public Long get() {
        return preferences.getLong(key, defaultValue);
    }

    @Override
    public void set(@NonNull Long value) {
        preferences.edit().putLong(key, value).apply();
    }
}
