package com.anadea.svv.sunnyday.features.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anadea.svv.sunnyday.R;
import com.anadea.svv.sunnyday.data.model.location.LocationInfo;
import com.anadea.svv.sunnyday.data.model.weather.adapter.WeatherInfoModel;
import com.anadea.svv.sunnyday.features.customize_dialog.LocationsCustomizeDialog;
import com.anadea.svv.sunnyday.features.main.forecast.ForecastPagerAdapter;
import com.anadea.svv.sunnyday.mvp.MvpActivity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpActivity<MvpMainContract.View, MvpMainContract.Presenter> implements
        MvpMainContract.View,
        SwipeRefreshLayout.OnRefreshListener,
        MenuItem.OnMenuItemClickListener {

    private static final String TAG = "MainActivity";

    public static final int MENU_ITEM_ID_AUTODETECT = 65;
    public static final int MENU_ITEM_ID_CUSTOMIZE = 730;

    public static final int PERMISSION_REQUEST_LOCATION = 76;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 986;

    @BindView(R.id.main_nav_view)
    NavigationView navigationView;
    @BindView(R.id.main_drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.weatherIcon_imageView)
    ImageView weatherIcon;
    @BindView(R.id.city_textView)
    TextView cityTextView;
    @BindView(R.id.date_textView)
    TextView dateTextView;
    @BindView(R.id.temp_textView)
    TextView tempTextView;
    @BindView(R.id.maxTemp_textView)
    TextView maxTempTextView;
    @BindView(R.id.minTemp_textView)
    TextView minTempTextView;
    @BindView(R.id.humidity_textView)
    TextView humidityTextView;
    @BindView(R.id.wind_textView)
    TextView windTextView;
    @BindView(R.id.description_textView)
    TextView descriptionTextView;
    @BindView(R.id.forecast_viewPager)
    ViewPager forecastViewPager;
    @BindView((R.id.add_to_favorites_fab))
    FloatingActionButton addToFavButton;
    @BindViews({R.id.location_imageView, R.id.maxTemp_imageView, R.id.minTemp_imageView})
    List<ImageView> weatherInfoImageViews;

    TextView navHeaderCityTextView;
    TextView navHeaderCountryTextView;
    TextView navHeaderLatLongTextView;

    ForecastPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        navHeaderCityTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_city_textView);
        navHeaderCountryTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_country_textView);
        navHeaderLatLongTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_latlong_textView);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_purple, android.R.color.holo_green_light, android.R.color.holo_blue_light);

        TabLayout tabLayout = findViewById(R.id.forecast_tabs);
        if (tabLayout != null)
            tabLayout.setupWithViewPager(forecastViewPager);
        pagerAdapter = new ForecastPagerAdapter(getSupportFragmentManager());
        forecastViewPager.setAdapter(pagerAdapter);
    }

    @Override
    protected MvpMainContract.Presenter createRetainedComponent() {
        return new MainPresenter();
    }

    @OnClick(R.id.drawer_menu_button)
    public void onMenuButtonClick() {
        drawerLayout.openDrawer(Gravity.START, true);
    }

    @OnClick(R.id.toolbar_search_button)
    public void onSearchButtonClick() {
        getPresenter().onSearchButtonClick(this, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @OnClick(R.id.add_to_favorites_fab)
    public void onAddToFavClick () {
        getPresenter().onAddToFavButtonClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);

                LocationInfo locationInfo = new LocationInfo(place.getName().toString(), null,
                        place.getLatLng().latitude, place.getLatLng().longitude);
                getPresenter().onNewGooglePlaceSelected(locationInfo);
            }
            else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e(TAG, status.getStatusMessage());

            }
            else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED)
                    return;
            }

            getPresenter().onLocationPermissionsGranted();
        }
    }

    @Override
    public void onRefresh() {
        getPresenter().onRefreshRequested();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void checkLocationPermissions() {

        if (Build.VERSION.SDK_INT < 23 ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getPresenter().onLocationPermissionsGranted();
        } else
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void updateNavigationDrawerMenu(List<LocationInfo> userLocationsList) {

        Menu menu = navigationView.getMenu();
        menu.clear();

        int order = 0;

        menu.add(0, MENU_ITEM_ID_AUTODETECT, order++, R.string.menu_item_label_autodetect)
                .setIcon(R.drawable.ic_my_location_black_24dp)
                .setOnMenuItemClickListener(this);

        for (int i = 0; i < userLocationsList.size(); i++) {
            LocationInfo locationInfo = userLocationsList.get(i);
            menu.add(0, locationInfo.getCityName().hashCode(), order++, locationInfo.getCityName())
                    .setIcon(R.drawable.ic_place_black_24dp)
                    .setOnMenuItemClickListener(this);
        }

        menu.add(1, MENU_ITEM_ID_CUSTOMIZE, order, R.string.menu_item_label_customize)
                .setOnMenuItemClickListener(this);

    }

    @Override
    public void updateNavHeaderLocationData(LocationInfo locationInfo) {
        navHeaderCityTextView.setText(locationInfo.getCityName());
        navHeaderCountryTextView.setText(locationInfo.getCountryName());
        navHeaderLatLongTextView.setText(locationInfo.getLatLong());
    }

    @Override
    public void updateCurrentWeatherData(WeatherInfoModel weatherInfoModel) {

        Locale locale = Locale.getDefault();

        ButterKnife.apply(weatherInfoImageViews, VISIBILITY, View.VISIBLE);

        Picasso.with(this)
                .load(weatherInfoModel.getIconPath())
                .into(weatherIcon);
        cityTextView.setText(weatherInfoModel.getLocationName());
        dateTextView.setText(weatherInfoModel.getWeatherDateExt());
        tempTextView.setText(weatherInfoModel.getTemperature());
        maxTempTextView.setText(weatherInfoModel.getMaxTemperature());
        minTempTextView.setText(weatherInfoModel.getMinTemperature());
        humidityTextView.setText(String.format(locale, "%s: %s",
                getString(R.string.str_label_humidity), weatherInfoModel.getHumidity()));
        windTextView.setText(String.format(locale, "%s: %s %s", getString(R.string.str_label_wind),
                weatherInfoModel.getWindSpeed(), getString(R.string.str_label_m_sec)));
        descriptionTextView.setText(weatherInfoModel.getDescription());
    }

    @Override
    public void setAddToFavButtonVisible(boolean enableAddToFav) {

        if (enableAddToFav)
            addToFavButton.show();
        else
            addToFavButton.hide();
    }

    @Override
    public void updateForecastDates(List<String> forecastDates) {
        pagerAdapter.updatePageTitles(forecastDates);
    }

    @Override
    public void showLocationsCustomizeDialog() {
        new LocationsCustomizeDialog().show(getSupportFragmentManager(), "Locations customize");
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        getPresenter().onNavMenuItemClick(menuItem);
        if (drawerLayout.isDrawerOpen(Gravity.START))
            drawerLayout.closeDrawer(Gravity.START);
        return true;
    }

    public static final ButterKnife.Setter<View, Integer> VISIBILITY = new ButterKnife.Setter<View, Integer>() {
        @Override
        public void set(@NonNull View view, Integer value, int index) {
            view.setVisibility(value);
        }
    };
}
