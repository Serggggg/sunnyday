package com.anadea.svv.sunnyday.data.remote;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class OpenWeatherInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        Request originalRequest = chain.request();
        HttpUrl originalUrl = originalRequest.url();

        HttpUrl newUrl = originalUrl.newBuilder()
                .addQueryParameter("units", "metric")
                .addQueryParameter("appid", "a60cc944cdf20c4aa04bf53c75a35a66")
                .addQueryParameter("lang", Locale.getDefault().getLanguage())
                .build();

        Request request = originalRequest.newBuilder().url(newUrl).build();

        return chain.proceed(request);
    }
}
