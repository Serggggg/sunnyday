package com.anadea.svv.sunnyday.preferences;

public interface Preference<T> {

    T get();
    boolean isSet();
    void set(T value);
    void delete();
}
