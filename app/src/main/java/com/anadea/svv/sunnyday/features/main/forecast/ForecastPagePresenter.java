package com.anadea.svv.sunnyday.features.main.forecast;

import android.support.annotation.NonNull;
import android.util.Log;

import com.anadea.svv.sunnyday.SunnyDayApp;
import com.anadea.svv.sunnyday.data.DataManager;
import com.anadea.svv.sunnyday.data.model.errors.ErrorMessage;
import com.anadea.svv.sunnyday.data.model.weather.WeatherInfo;
import com.anadea.svv.sunnyday.mvp.core.BasePresenter;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.functions.Consumer;

public class ForecastPagePresenter extends BasePresenter<MvpForecastContract.View>
        implements MvpForecastContract.Presenter {

    private static final String TAG = "ForecastPagePresenter";

    @Singleton
    @Inject
    DataManager dataManager;

    private final int pageNum;

    ForecastPagePresenter(int pageNum) {
        SunnyDayApp.getAppComponent().inject(this);
        this.pageNum = pageNum;
    }

    @Override
    public void attachView(@NonNull MvpForecastContract.View view) {
        super.attachView(view);

        // Subscribe to forecast info updates
        compositeDisposable
                .add(dataManager.subscribeForecastPage(new Consumer<Map<String, List<WeatherInfo>>>() {
                    @Override
                    public void accept(Map<String, List<WeatherInfo>> forecastMap) throws Exception {
                        if (forecastMap != null) {
                            String[] keys = forecastMap.keySet().toArray(new String[forecastMap.size()]);
                            getView().updateForecastData(forecastMap.get(keys[pageNum]));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        ErrorMessage errorMessage = dataManager.parseErrorMessage(throwable);
                        Log.e(TAG, errorMessage.getMessage());
                    }
                }));
    }

}
