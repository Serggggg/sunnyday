package com.anadea.svv.sunnyday.mvp.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity<T> extends AppCompatActivity {

    protected T mvpComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //noinspection unchecked
        mvpComponent = (getLastCustomNonConfigurationInstance() == null)
                ? createRetainedComponent()
                : (T) getLastCustomNonConfigurationInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        notifyComponentOnDestroy(isChangingConfigurations());
        mvpComponent = null;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mvpComponent;
    }

    abstract protected T createRetainedComponent();

    abstract protected void notifyComponentOnDestroy(boolean isChangingConfigurations);
}
