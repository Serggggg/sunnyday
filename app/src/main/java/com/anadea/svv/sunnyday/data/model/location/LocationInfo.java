package com.anadea.svv.sunnyday.data.model.location;

import android.support.annotation.NonNull;

import java.util.Locale;

public class LocationInfo implements Comparable<LocationInfo> {

    private final String cityName;
    private final String countryName;
    private final double latitude;
    private final double longitude;

    public LocationInfo(String cityName, String countryName, double latitude, double longitude) {
        this.cityName = cityName;
        this.countryName = countryName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static LocationInfo createEmpty() {
        return new LocationInfo(null, null, 0, 0);
    }

    public boolean isEmpty() {
        return cityName == null && countryName == null && latitude == 0 && longitude == 0;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getLatLong() {
        return String.format(Locale.getDefault(), "%.6f, %.6f", latitude, longitude);
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "LocationInfo: %s, %s [%.6f, %.6f]",
                cityName, countryName, latitude, longitude);
    }

    @Override
    public int compareTo(@NonNull LocationInfo info) {
        return getCityName().compareTo(info.getCityName());
    }
}
