package com.anadea.svv.sunnyday.di.components;

import com.anadea.svv.sunnyday.data.DataManager;
import com.anadea.svv.sunnyday.di.modules.AppModule;
import com.anadea.svv.sunnyday.di.modules.DataModule;
import com.anadea.svv.sunnyday.di.modules.LocationModule;
import com.anadea.svv.sunnyday.di.modules.PreferencesModule;
import com.anadea.svv.sunnyday.di.modules.RestModule;
import com.anadea.svv.sunnyday.features.customize_dialog.LocationsCustomizePresenter;
import com.anadea.svv.sunnyday.features.main.MainPresenter;
import com.anadea.svv.sunnyday.features.main.forecast.ForecastPagePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DataModule.class, RestModule.class, PreferencesModule.class, LocationModule.class})
public interface AppComponent {

    void inject(DataManager dataManager);

    void inject(MainPresenter presenter);

    void inject(ForecastPagePresenter presenter);

    void inject(LocationsCustomizePresenter presenter);
}
