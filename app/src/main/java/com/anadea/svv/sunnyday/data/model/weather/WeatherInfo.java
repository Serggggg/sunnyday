package com.anadea.svv.sunnyday.data.model.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeatherInfo {

    @SerializedName("weather")
    @Expose
    private List<Description> descriptionList = new ArrayList<>();

    @SerializedName("main")
    @Expose
    private Common common;

    @SerializedName("visibility")
    @Expose
    private int visibility;

    @SerializedName("wind")
    @Expose
    private Wind wind;

    @SerializedName("dt")
    @Expose
    private long timestamp;

    @SerializedName("sys")
    @Expose
    private Solar solar;

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public Common getCommon() {
        return common;
    }

    public int getVisibility() {
        return visibility;
    }

    public Wind getWind() {
        return wind;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Solar getSolar() {
        return solar;
    }

}
